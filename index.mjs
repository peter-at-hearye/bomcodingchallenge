// 'use strict';   // Not actually needed in mjs files

// Identification and splash message before ANYTHING else happens:
const me = 'index.mjs'   // (__filename gone in ES modules! Boooo!)
console.log(`${me}: Starting...`)


/* eslint-disable import/first */
import express from 'express'
import bomFilter from './src/bom-filter.mjs'


// Stand up Express app:
const PORT = Number(process.env.PORT) || 3000
export const app = express()
app.use(express.json())
app.get('/', bomFilter)

// A guard for Jasmine-Supertest testing - to not start the server listening:
if (process.env.OFFLINE_TEST) {
    console.warn("OFFLINE_TEST mode. NOT starting server listen!")
} else {
    app.listen(PORT, () => console.log(`${me}: Listening on port ${PORT}`))
        .on('error', (err) => {
            console.error(`${me}: Error from web service:`)
            console.error(err)
        })
}
