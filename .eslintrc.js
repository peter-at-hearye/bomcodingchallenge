
module.exports = {
    env: {
        es2021: true,
        jasmine: true
    },
    extends: [
        'standard'
    ],
    parserOptions: {
        ecmaVersion: 12,
        sourceType: 'module'
    },
    rules: {
        /* eslint-disable quote-props */
        "import-first": 0,
        "indent": ['error', 4],
        "no-multiple-empty-lines": ["error", { "max": 2, "maxEOF": 1 }],
        "padded-blocks": 0,
        "quotes": 0,
        "no-multi-spaces": ["error", { ignoreEOLComments: true }],
        "space-before-function-paren": 0
    },
    overrides: [
        {
            files: ["**/*.mjs"]
        }
    ]
}
