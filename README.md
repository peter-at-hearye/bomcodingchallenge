
# BOM Filter service

Service built to requirements, to query the Bureau of Meteorology and return selected data.  
**(See bottom of README for notes on the challenge solution)**

----
## See it in action
You can see the output of this application now via browser/curl at:

https://bomcodingchallenge.herokuapp.com/

----
## Get the code
The code repository can be accessed publicly on BitBucket. See the project at:  
[https://bitbucket.org/peter-at-hearye/bomcodingchallenge](https://bitbucket.org/peter-at-hearye/bomcodingchallenge)

Or clone the project for local inspection with git:
```bash
$ git clone https://peter-at-hearye@bitbucket.org/peter-at-hearye/bomcodingchallenge.git
```

----
## Setup
**See ```setup.sh``` for useful setup commands, and other commands.**

NodeJS 14 is recommended, as the solution was developed in this latest stable 'LTS' version.

At a minimum, for local development, start with the usual:
```bash
$ npm ci   # Fast, clean install based on package-lock.json file
```

**Note: Some commands in package.json assume a unix shell.  
Development on Windows may require some minor alterations.**

### Linting and Tests:
```bash
$ npm run lint    # Via ESLint
$ npm run test    # Via Jasmine
```

### Service can be run locally:
```bash
$ npm run debug   # Run and restart on any changes
$ npm run start   # Vanilla start
$ xdg-open http://localhost:3000   # Open browser on unix. Otherwise just open the URL!
```

### Or on Heroku (once installed and logged into Heroku):
```bash
$ npm run herokudeploy
$ heroku open
```

### Or via Docker (if Docker available locally):
```bash
$ npm run dockerdeploy
$ xdg-open http://localhost:3000   # Open browser on unix. Otherwise just open the URL!
```

----
# NOTES on solution to the challenge
**The requirements for the challenge stated "should be written using NodeJS", so I decided to avoid the extra boilerplate of Typescript, and use Javascript with ES Modules as a bit of a learning exercise (hence *.mjs files).**

**I would use Typescript for my next production project though, as I found support for ES modules still a bit patchy - Only a minority of test frameworks (Jasmine?) officially support ES modules!**

**Testing is done at the http service level via jasmine + supertest, hence is testing as much as possible without an actual external http test. Unit tests could be added for the main src module ```bom-filter.mjs```, but they would look a lot like the existing tests in ```index.test.mjs```, so were skipped for this exercise.**

**As noted above: The solution is currently targeted at a unix development environment. Sorry for Windows devs out there - Some minor mods would be needed!**

**_NOTE: The requirements mentioned "lat" and "long" but the BOM data actually has "lat" and "lon", so I used "lon" since it said "fields from the request". I am assuming that was a typo or auto-correct, but if it was a trick question... You may have tricked me!_**

**Cheers!**