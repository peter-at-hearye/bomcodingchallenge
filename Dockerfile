# syntax=docker/dockerfile:1

FROM node:14.17.3

WORKDIR /app

COPY . .

# Clean/Fast install only production dependencies:
RUN npm ci --production

ENV NODE_ENV=production
ENV PORT=3000
EXPOSE 3000
USER node
CMD [ "node", "index.mjs" ]
