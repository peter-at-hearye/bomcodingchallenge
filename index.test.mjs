/* eslint-disable space-before-function-paren */
/** We will use supertest to test HTTP requests/responses */
import request from 'supertest'

/** Express 'app' object from index.mjs */
import { app } from './index.mjs'


// Facility to keep error output quiet when running tests that we expect to throw errors:
const oldError = console.error
function quiteErrors() { console.error = () => { } }
function normalErrors() { console.error = oldError }


/** Convenience function for running the request, being quiet on errors, etc: */
async function doErroneousRequestQuietly() {
    quiteErrors()
    const appResponse = await request(app).get("/")
    // console.log(`Got response:`, appResponse)
    normalErrors()
    delete process.env.BOM_URL
    delete process.env.BOM_TEMP_THRESH
    return appResponse
}


describe("BOM Filter service", function () {
    let appResponse

    beforeAll(async function () {
        appResponse = await request(app).get("/")
        // console.log(`Got response:`, appResponse)
    })

    it("should return status 200", function () {
        expect(appResponse?.statusCode).toEqual(200)
    })

    it("should only return observations above temperature threshold (10.0)", function () {
        const found = appResponse?.body.response.find(o => o.apparent_t <= 10.0)
        expect(found).toBe(undefined)
    })

    it("should return observations in ascending order", function () {
        const obs = appResponse?.body.response
        for (let i = 1; i < obs.length; ++i) {
            expect(obs[i - 1].apparent_t <= obs[i].apparent_t).toBe(true)
        }
    })

})


describe("BOM Filter service Edge Cases", function () {

    it("should handle zero results", async function () {
        // Observations above 99 degrees celcius should give us zero results:
        process.env.BOM_TEMP_THRESH = 99
        const appResponse = await doErroneousRequestQuietly()
        // console.log(`Got response:`, appResponse)

        expect(appResponse?.statusCode).toEqual(200)
        expect(Array.isArray(appResponse?.body?.response)).toBe(true)
        expect(appResponse?.body?.response?.length).toBe(0)
    })

})


describe("BOM Filter service Errors", function () {

    it("should return error on bad URL path", async function () {

        process.env.BOM_URL = "http://www.bom.gov.au/fwo/IDN60801/IDN60801.95765.json.RANDOM_GARBAGE"

        const appResponse = await doErroneousRequestQuietly()

        expect(appResponse?.statusCode).toEqual(503)
        expect(typeof appResponse?.body?.error).toEqual('string')
    })

    it("should return error on bad URL host", async function () {

        process.env.BOM_URL = "http://www.bom.badhost.gov.au/fwo/IDN60801/IDN60801.95765.json"

        const appResponse = await doErroneousRequestQuietly()

        expect(appResponse?.statusCode).toEqual(503)
        expect(typeof appResponse?.body?.error).toEqual('string')
    })

})
