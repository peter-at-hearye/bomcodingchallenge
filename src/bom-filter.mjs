// 'use strict'; // Not actually needed in mjs files, but no harm

import util from 'util'
import axios from 'axios'
import debug from 'debug'

const me = 'bcc:bom-filter'
/** Debug logger (Activated via DEBUG=* in environment) */
const LOG = debug(me)


/**
 * Express router function, to query and return BOM data
 * @param req Express Request object
 * @param res Express Response object
 */
export default async function bomFilter(req, res) {

    // Main BOM query parameters can be altered via environ: (Useful for testing)
    /** URL for BOM query: */
    const BOM_URL = process.env.BOM_URL || 'http://www.bom.gov.au/fwo/IDN60801/IDN60801.95765.json'
    /** The temperature threshold (> this is returned): */
    const BOM_TEMP_THRESH = Number(process.env.BOM_TEMP_THRESH) || 10.0


    try {
        const data = await doBomFilter(
            /** Where we obtain the BOM data, per requirements: */
            BOM_URL,
            BOM_TEMP_THRESH
        )
        LOG(`Got filtered and sorted obs: ${util.inspect(data, false, 99, true)}`)

        // The requirements request to "have a response key",
        //  which I intepret to mean a property named 'response', containing the data:
        res.json({ response: data })
    } catch (err) {
        console.error(`${me}: Error querying BOM:`)
        // Axios errors are very verbose with thes request/response objects, so remove them:
        delete err.request
        delete err.response
        console.error(err)
        res.status(503).json({ error: err.message })
    }
}

/**
 * Do the query to BOM and filter. Any error will throw.
 * @param url BOM URL to query for observation data
 * @param tempsAbove Threshold, to only return temps above this value
 * @returns A data object with the filtered and sorted observations
 */
export async function doBomFilter(url, tempsAbove) {

    const bomResponse = await axios.get(url)
    LOG(`Got raw BOM data: ${util.inspect(bomResponse.data, false, 99, true)}`)

    // Obtain the Observations data array, and sanity check:
    let obs = bomResponse?.data?.observations?.data
    if (!obs || !Array.isArray(obs) || obs.length < 1) {
        throw new Error('No observation data in BOM response')
    }

    // Now apply filtering and mapping per requirements:
    obs = obs
        .filter(o => typeof o === 'object')   // - Safety: Ensure "o." etc below will not error
        .filter(o => o.apparent_t > tempsAbove)
        // Map each observation down to the required subset of keys:
        .map(o => ({ name: o.name, apparent_t: o.apparent_t, lat: o.lat, lon: o.lon }))
        // Sort into ascending Temp order:
        .sort((a, b) => {
            if (a.apparent_t > b.apparent_t) return 1
            else if (a.apparent_t < b.apparent_t) return -1
            else return 0
        })

    return obs
}
