#!/bin/sh

#
# Take this more as EXAMPLES of some key setup and commands.
# But it may run for you! (Not tested!)
#

# NodeJS 14 is recommended, as the solution was developed in this latest stable 'LTS' version.

# Install Heroku if needed:
heroku --version || curl https://cli-assets.heroku.com/install.sh | sh
heroku login
heroku create bomcodingchallenge

# Install Node dependencies clean & fast via package-lock.json :
npm ci

npm run lint || exit 1
npm run test || exit 1

# Deploy to Heroku
npm run herokudeploy

# Run locally with DEBUG and auto-restart on save:
npm run debug

# Docker build & deploy:
npm run dockerdeploy

# Open in browser:
xdg-open http://localhost:3000
